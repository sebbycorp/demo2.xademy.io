module demo2.xademy.io

go 1.20

require (
	github.com/gohugoio/hugo-mod-bootstrap-scss/v5 v5.20300.20200 // indirect
	github.com/maniak-academy/training-theme-alpha v0.0.0-20240110181256-7e47e2a5554f // indirect
)
